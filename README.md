# lifet.py Lifetime fitting tools

This module contains tools for fitting fluorescence decay models to streak data
and TCSPC. Mostly thin wrappers over scipy.

Plans to include common models (exponential decays, gaussian distribution,
stretched exponential) and common procedures (tail fit, reconvolution fitting).

This module does not splice or select the data in any way, but may contain
methods to guess initial conditions.

Currently procedural. OO may be useful.

## Currently supported models

None

## Planned models

The following models are planned to be included, more or less in this order.

* Single exponential
* Multi exponential
* Stretched exponential
* Gaussian distribution

## Planned tools:

Streak specific:
* Spline interpolation of IRF and data
* Rebinning of model (integration with variable bounds)

Fitting methods
* Tail Fit
* Reconvolution fitting

Optimization algorithms:
* Powell
* Simplex
* Levenberg-Marquart

Error analysis:
* Residuals
* Residual autocorrelation (is that even needed?)
* error estimations ?(ASE, Covariance, Support plane, Bootstrap) see what Origin does + read

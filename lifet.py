# lifet.py  lifetime fitting utilities
"""
Created Tue May 19 2015

lifetime fitting tools

Functions useful in fitting decay models to streak camera data. Can also be used
for TCSPC results.

@author Samuel Palato
"""
# TODO:
# Fitting functions
# Cst spacing rebinning: should that be a decorator or inline?
# Non cst spacing integration
# models: exp decay
# test!
# Design to line usage?

from __future__ import print_function, division
import logging
from functools import wraps
import numpy as np
from scipy.optimize import minimize, leastsq
from scipy.interpolate import UnivariateSpline
from scipy.signal import fftconvolve

__author__ = 'Samuel Palato'

logger = logging.getLogger()


class OptimizationError(Exception):
    """
    Optimization failure. Optionnally has optimization results.
    """
    def __init__(self, msg, *args, **kwargs):
        try:
            res = kwargs.pop('res')
        except KeyError:
            res = None
        super(OptimizationError, self).__init__(self, *args, **kwargs)
        self.msg = msg
        if res:
            self.res = res
    def __str__(self):
        return "OptimizationError("+self.msg+")"

def residuals_of(model, x, y_obs, weights=1):
    """Function to compute weighted residuals of model for y_obs.

    Doesn't play fancy axis games (yet).

    Parameters
    ----------
    model   callable,
            model function
    y_obs   np.ndarray
            observed values
    weights scalar or np.ndarray
            weights of observations.

    Returns
    -------
    res     function
            Almost same callsig as `model`, but returns residuals vs y_obs
            instead. New function has one less parameter, the first, which
            should be indep. variable. ie: f(x, p0, p1..) -> f(p0, p1...)
    """
    # TODO: seems to work, but more intensive tests required
    @wraps(model)
    def res(*args, **kwargs):
        return weights *(y_obs - model(x, *args, **kwargs))
    return res


def chi2_of(model, x, y_obs, ddof, weights=1):
    """Function to compute the reduced chi2 of model vs y_obs.

    Doesn't play fancy axis games (yet).

    Parameters
    ----------
    model   callable
            model function
    y_obs   np.ndarray
            observed values
    ddof    int
            number of fitted parameters
    weights scalar or np.ndarray
            weights of residuals

    Returns
    -------
    chi2    function
            same callsig as model, but returns reduced chi2 instead.
            Almost same callsig as `model`, but returns reduced chi2 instead
            New function has one less parameter, the first, which should be
            indep. variable. ie: f(x, p0, p1..) -> f(p0, p1...)
    """
    # TODO: seems to work, but more intensive tests required
    resf = residuals_of(model, x, y_obs, weights)
    @wraps(model)
    def chi2(*args, **kwargs):
        wr = resf(*args, **kwargs)
        return np.sum(wr**2)/(wr.size - ddof)
    return chi2


def reconvolve(model, irffunc):
    """
    Makes a reconvolved model function.

    Parameters
    ----------
    model   callabe. Must have indep variable as first parameter
            model function
    irffunc callable
            irf function

    Returns
    -------
    reconv  callable
            same callsig as model, model reconvolved with irf
    """
    # TODO: swap to data points?
    @wraps(model)
    def reconv(x, *args, **kwargs):
        y = model(x, *args, **kwargs)
        return fftconvolve(y, irffunc(x), mode='full')[:y.size]
    return reconv


def unpack_args(func):
    """
    Changes callsig from func(vec) to func(arg0, arg1, arg2)...

    Scipy.optimization use a single vector, we want to use args.
    """
    def unpacked(vec, *extra, **kw):
        args = vec.tolist()
        args.extend(extra)
        return func(*args, **kw)
    return unpacked


def lifet_fit(model, xdata, yobs, p0, weights=1, irf=None, method='Powell',
              full_output=False, min_kwargs={}):
    """
    Fit a lifetime model to the data.

    Parameters
    ----------
    model   callable, model to fit.
            Assumes yobs = model(xdata, *p0) + eps
    xdata   np.ndarray, indep variable
    yobs    np.ndarray, observed variables
    p0      initial guess parameters
    weights     scalar or np.ndarray
            weights for the residuals. Defaults to 1.
    irf     np.ndarray, irf data for reconvolution fitting.
            The IRF data should match the x axis. Reconvolution assumes constant
            spacing in x. Default to None, which results in a tail fit.
    method  String. Optimization method.
            Any method recognized by scipy.optimize.minimize or 'LM'. In the
            latter case, scipy.optimize.leastsq is used instead.
            Defaults to Powell.
    full_output  bool, defaults to False
            If True, returns all optional outputs. Otherwise, returns only
            optimal parameters and chi2.
    min_kwargs  dict, extra kwargs for the minimize
            See scipy.optimize.leastsq or scipy.optimize.minimize depending on
            your method choice.

    Returns
    -------
    optp    np.ndarray, optimal parameters
    chi2    float, reduced chi2 value
    extra   TODO: document
    """
    # TODO: seems to work
    p0 = np.asarray(p0)
    if irf != None:
        irf_func = UnivariateSpline(xdata, irf/np.sum(irf), k=1, s=0)
        model = reconvolve(model, irf_func)
        min_kwargs.update({'method': method})
    if method == 'LM':
        target = residuals_of(model, xdata, yobs, weights=weights)
        min_kwargs.update({'full_output': 1})
    else:
        target = chi2_of(model, xdata, yobs, p0.size, weights=weights)
    target = unpack_args(target)
    extra = {'model': model}
    if method == 'LM':
        # maybe I should use curve_fit instead
        optp, cov_x, info, mesg, ier = leastsq(target, p0, **min_kwargs)
        if ier not in [1, 2, 3, 4]:
            raise OptimizationError(mesg, res=info)
        wres = target(optp)
        dof = wres.size - optp.size
        chi2 = np.sum(wres*wres)/dof
        extra.update({'cov_x': cov_x, 'info': info, 'mesg': mesg, 'ier': ier})
    else:
        res = minimize(target, p0, **min_kwargs)
        if not res.success:
            raise OptimizationError(res.message, res=res)
        optp = res.x
        chi2 = res.fun
        extra.update(res)
    if full_output:
        return optp, chi2, extra
    else:
        return optp, chi2




def exp_decay(t, amp, tau, t0=0, baseline=0):
    """
    Single exponential decay. t0 and baseline can be kept constant.

    y = amp * exp(-(t-t0)/tau) + baseline

    Parameters
    ----------
    t       np.ndarray
        Independant variable
    amp     float
        Amplitude
    tau     float
        1/e time
    t0  float. Optional, defaults to 0
        Delay. y=0 for t<t0
    baseline    float, Optional, defaults to 0
        Minimum value.
    """
    y = amp * np.exp(-(t-t0)/tau)
    y[t<t0] = 0
    y += baseline
    return y


def double_exp(t, amp0, tau0, amp1, tau1, t0=0, baseline=0):
    """
    Double exponential decay.  t0 and baseline can be kept constant.
    """
    return (exp_decay(t, amp0, tau0, t0) +
            exp_decay(t, amp1, tau1, t0)) + baseline

# TODO: try multi_exp(*args, t0=0, baseline=0) and parse args?
def triple_exp(t, amp0, tau0, amp1, tau1, amp2, tau2, t0=0, baseline=0):
    """
    Triple exponential decay, t0 and baseline can be kept constant.
    """
    return (exp_decay(t, amp0, tau0, t0) +
            exp_decay(t, amp1, tau1, t0) +
            exp_decay(t, amp2, tau2, t0)) + baseline


def stretched_exp(t, amp, tau, beta, t0=0, baseline=0):
    """
    Strectched exponential decay, or Kohlrausch-Williams-Watt law

    y = amp* exp(-((t-t0)/tau)**beta) + baseline

    Parameters
    ----------
    t       np.ndarray
        Independant variable
    amp     float
        Amplitude
    tau     float
        characteristic time
    beta    float
        Kohlrausch exponent
    t0      float. Optional, defaults to 0
        Delay. y=0 for t<t0
    baseline    float, optional, defaults to 0
        Minimum value
    """
    y = amp*np.exp(-((t-t0)/tau)**beta)
    y[t<t0] = 0
    y += baseline
    return y

